import { Controller } from 'stimulus';

export default class extends Controller {
    static values = { url: String, refreshInterval: Number };
    static targets = ['counter'];
    connect() {
        this.load()
        if (this.hasRefreshIntervalValue) {
            this.startRefreshing()
        }
    }
    load() {
        fetch(this.urlValue)
            .then(response => response.text())
            .then(obj => this.counterTarget.textContent = obj)
    }
    startRefreshing() {
        setInterval(
            () => this.load(),
            this.refreshIntervalValue
        )
    }
}
