<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210227003513 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE "comments_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "news_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "comments" (id INT NOT NULL, user_id INT DEFAULT NULL, parent_id INT DEFAULT NULL, article_id INT DEFAULT NULL, text TEXT NOT NULL, publish_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5F9E962AA76ED395 ON "comments" (user_id)');
        $this->addSql('CREATE INDEX IDX_5F9E962A727ACA70 ON "comments" (parent_id)');
        $this->addSql('CREATE INDEX IDX_5F9E962A7294869C ON "comments" (article_id)');
        $this->addSql('CREATE TABLE "news" (id INT NOT NULL, author_id INT NOT NULL, title VARCHAR(255) NOT NULL, preview_text VARCHAR(255) NOT NULL, detail_text TEXT NOT NULL, publish_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, preview_image VARCHAR(255) NOT NULL, detail_image VARCHAR(255) NOT NULL, attachments JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1DD39950F675F31B ON "news" (author_id)');
        $this->addSql('ALTER TABLE "comments" ADD CONSTRAINT FK_5F9E962AA76ED395 FOREIGN KEY (user_id) REFERENCES "users" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "comments" ADD CONSTRAINT FK_5F9E962A727ACA70 FOREIGN KEY (parent_id) REFERENCES "comments" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "comments" ADD CONSTRAINT FK_5F9E962A7294869C FOREIGN KEY (article_id) REFERENCES "news" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "news" ADD CONSTRAINT FK_1DD39950F675F31B FOREIGN KEY (author_id) REFERENCES "users" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE "comments" DROP CONSTRAINT FK_5F9E962A727ACA70');
        $this->addSql('ALTER TABLE "comments" DROP CONSTRAINT FK_5F9E962A7294869C');
        $this->addSql('DROP SEQUENCE "comments_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE "news_id_seq" CASCADE');
        $this->addSql('DROP TABLE "comments"');
        $this->addSql('DROP TABLE "news"');
    }
}
