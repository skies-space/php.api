const Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath('public/admin.build/')// directory where compiled assets will be stored
    .setPublicPath('/admin.build')

    .addEntry('app', './assets.admin/app.js')
    .addStyleEntry('tailwind', './assets.admin/styles/tailwind.css')
    .enableStimulusBridge('./assets.admin/controllers.json')// enables the Symfony UX Stimulus bridge (used in assets/bootstrap.js)

    .splitEntryChunks()// When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())// enables hashed filenames (e.g. app.abc123.css)

    .enablePostCssLoader((options) => {
        options.postcssOptions = {
            config: './postcss.config.js',
        };
    })
    .configureBabel((config) => {
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })    // enables @babel/preset-env polyfills
module.exports = Encore.getWebpackConfig();
