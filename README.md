# Новостной портал
- документация к api
    - хранится в `public/api_docs.json`
    - генерируется командой `symfony console api:openapi:export --output 'public/api_docs.json'`
- регистрация администратора `symfony console CreateAdmin testadmin testadmin@mail.com FW78mWxSYrW`

## Разворот проекта
- Для запуска  с использованием web-сервера
    - Создать хост в директорию `public/`
    - Настроить rewrite на `public/index.php`
- Для запуска  лоакального web-сервера
    - В корне проекта выполнить `symfiny server:start -d`

    
- Для запуска с использованием docker (необходимо тестирование, доработка)
    - скопировать `docker/docker-compose.yaml` в корень прокта
    - выполнить `docker-compose build` в корне прокта
    - выполнить `docker-compose up -d` в корне прокта
- Для запуска  с использованием базы данных в docker (работает)
    - выполнить `docker-compose up -d` в корне прокта

### Инициализация проекта
Данные шаги необходимы для запуска проекта локально, на web-сервере без docker или с использованием только бд в docker.

- Копировать `./.env.sample` как `./.env.local`
  - заполнить настройки в `./.env.local` актуальной информацией
    - расскоментировать и заполнить `DATABASE_URL`
- выполнить команду `symfony composer install`
- выполнить команду `symfony console doctrine:migrations:migrate`
- выполнить команду `symfony console lexik:jwt:generate-keypair --overwrite`
  - или выполнить
  - `openssl genpkey -out config/jwt/private.pem -pass stdin -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096`
  - `openssl pkey -in config/jwt/private.pem -passin stdin -out config/jwt/public.pem -pubout`
