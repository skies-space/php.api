<?php
namespace App\Admin\EventSubscriber;

use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserPlainPasswordSubscriber implements EventSubscriberInterface
{
    public function __construct(private UserPasswordEncoderInterface $_passwordEncoder){}

    public static function getSubscribedEvents(): array
    {
        return [
            BeforeEntityPersistedEvent::class => ['setEncodedPassword'],
            BeforeEntityUpdatedEvent::class => ['setEncodedPassword']
        ];
    }

    public function setEncodedPassword(BeforeEntityPersistedEvent|BeforeEntityUpdatedEvent $event): void
    {
        $user = $event->getEntityInstance();
        if(in_array(UserInterface::class,class_implements($user))){
            if(!empty($user->getPlainPassword())){
                $user->setPassword($this->_passwordEncoder->encodePassword($user, $user->getPlainPassword()));
            }
        }
    }
}
