<?php

namespace App\Validator;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ParentsNestRestrictionValidator extends ConstraintValidator
{
    private $em;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint ParentsNestRestriction */

        if (null === $value || '' === $value) {
            return;
        }
        if(!method_exists($value::class, 'getParent')){
            $this->context->buildViolation($constraint->message_childrens_method_notexists)
                ->setParameter('{{ value }}', $value::class)
                ->addViolation();
            return;
        }
        $comment_parent = $value->getParent();
        $this->em->initializeObject($comment_parent);
        if(!empty($comment_parent)){
            $nest_chain = [$value, $comment_parent];
            while($parent = $comment_parent->getParent()){
                $this->em->initializeObject($parent);
                $nest_chain[] = $comment_parent = $parent;
                if(count($nest_chain) > 5){
                    $this->context->buildViolation($constraint->message)->addViolation();
                    return;
                }
            }
        }
    }
}
