<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ParentsNestRestriction extends Constraint
{
    /*
     * Any public properties become valid options for the annotation.
     * Then, use these in your validator class.
     */
    public $message = 'China!';
    public $message_childrens_method_notexists = 'Entity "{{ value }}" not contains getParent method.';
}
