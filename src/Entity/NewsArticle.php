<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\NewsArticleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=NewsArticleRepository::class)
 * @ORM\Table(name="`news`")
 * @ORM\HasLifecycleCallbacks()
 */
#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ["get"],
    shortName: 'news',
    normalizationContext: ['groups' => ['news_article']],
    paginationClientEnabled: true,
    paginationClientItemsPerPage: true,

)]
class NewsArticle
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    #[Groups(["news_article", "comment_post"])]
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Groups(["news_article"])]
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Groups(["news_article"])]
    private $previewText;

    /**
     * @ORM\Column(type="text")
     */
    #[Groups(["news_article"])]
    private $detailText;

    /**
     * @ORM\Column(type="datetime")
     */
    #[Groups(["news_article"])]
    private $publishDate;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="newsArticles")
     * @ORM\JoinColumn(nullable=false)
     */
    #[Groups(["news_article"])]
    #[ApiProperty(readableLink: false, writableLink: false)]
    private $author;

    /**
     * @ORM\OneToMany(targetEntity=NewsArticleComment::class, mappedBy="article", cascade="remove")
     */
    private $newsArticleComments;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Groups(["news_article"])]
    #[SerializedName("previewImage")]
    private $preview_image;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Groups(["news_article"])]
    #[SerializedName("detailImage")]
    private $detail_image;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
//    #[Groups(["news_article"])]
    private $attachments = [];

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->newsArticleComments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPreviewText(): ?string
    {
        return $this->previewText;
    }

    public function setPreviewText(string $previewText): self
    {
        $this->previewText = $previewText;

        return $this;
    }

    public function getDetailText(): ?string
    {
        return $this->detailText;
    }

    public function setDetailText(string $detailText): self
    {
        $this->detailText = $detailText;

        return $this;
    }

    public function getPublishDate(): ?\DateTimeInterface
    {
        return $this->publishDate;
    }

    public function setPublishDate(\DateTimeInterface $publishDate = null): self
    {
        if (!is_null($publishDate)) {
            $this->publishDate = $publishDate;
        } else {
            $this->publishDate = new \DateTime("now");
        }

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function preparePublishDate(): void
    {
        $this->publishDate = new \DateTime("now");
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|NewsArticleComment[]
     */
    public function getNewsArticleComments(): Collection
    {
        return $this->newsArticleComments;
    }

    public function addNewsArticleComment(NewsArticleComment $newsArticleComment): self
    {
        if (!$this->newsArticleComments->contains($newsArticleComment)) {
            $this->newsArticleComments[] = $newsArticleComment;
            $newsArticleComment->setArticle($this);
        }

        return $this;
    }

    public function removeNewsArticleComment(NewsArticleComment $newsArticleComment): self
    {
        if ($this->newsArticleComments->removeElement($newsArticleComment)) {
            // set the owning side to null (unless already changed)
            if ($newsArticleComment->getArticle() === $this) {
                $newsArticleComment->setArticle(null);
            }
        }

        return $this;
    }

    public function getPreviewImage(): ?string
    {
        return $this->preview_image;
    }

    public function setPreviewImage(string $preview_image): self
    {
        $this->preview_image = $preview_image;

        return $this;
    }

    public function getDetailImage(): ?string
    {
        return $this->detail_image;
    }

    public function setDetailImage(string $detail_image): self
    {
        $this->detail_image = $detail_image;

        return $this;
    }

    public function getAttachments(): ?array
    {
        return $this->attachments;
    }

    public function setAttachments(?array $attachments): self
    {
        $this->attachments = $attachments;

        return $this;
    }
}
