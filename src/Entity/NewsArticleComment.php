<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\NumericFilter;
use App\Repository\CommentRepository;
use App\Validator\ParentsNestRestriction;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=CommentRepository::class)
 * @ORM\Table(name="`comments`")
 * @ORM\HasLifecycleCallbacks()
 */
#[ApiResource(
    collectionOperations: [
        'get'  => ["security" => "is_granted('ROLE_READER')"],
        'post' => ["security" => "is_granted('ROLE_COMMENTATOR')"],
    ],
    itemOperations: [
        "get" => ["security" => "is_granted('ROLE_READER')"]
    ],
    shortName: 'comments',
    denormalizationContext: ['groups' => ['comment_post']],
    normalizationContext: ['groups' => ['comment_get']],
    paginationClientEnabled: true,
    paginationClientItemsPerPage: true
)]
#[ApiFilter(NumericFilter::class, properties: ['article.id' => 'exact'])]
class NewsArticleComment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"comment_get","comment_post"})
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Groups({"comment_get","comment_post"})
     */
    private $text;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"comment_get","comment_post"})
     */
    private $publishDate;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="comments")
     * @ORM\JoinTable(
     *     name="users",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)},
     * )
     * @Groups({"comment_get"})
     */
    #[ApiProperty(readableLink: false, writableLink: false)]
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=NewsArticleComment::class, inversedBy="childrens")
     * @Groups({"comment_post"})
     * @ParentsNestRestriction()
     */
    #[ApiProperty(readableLink: false, writableLink: true)]
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity=NewsArticleComment::class, mappedBy="parent", cascade="remove")
     * @Groups({"comment_get"})
     */
    #[ApiProperty(readableLink: true, writableLink: false)]
    private $childrens;

    /**
     * @ORM\ManyToOne(targetEntity=NewsArticle::class, inversedBy="newsArticleComments")
     * @Groups({"comment_post"})
     */
    #[ApiProperty(readableLink: false, writableLink: true)]
    private $article;

    public function __construct()
    {
        $this->childrens = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getPublishDate(): ?\DateTimeInterface
    {
        return $this->publishDate;
    }

    public function setPublishDate(\DateTimeInterface $publishDate): self
    {
        if(!is_null($publishDate)) {
            $this->publishDate = $publishDate;
        }
        else {
            $this->publishDate = new \DateTime("now");
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function preparePublishDate(): void
    {
        $this->publishDate = new \DateTime("now");
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getChildrens(): Collection
    {
        return $this->childrens;
    }

    public function addChildren(self $children): self
    {
        if (!$this->childrens->contains($children)) {
            $this->childrens[] = $children;
            $children->setParent($this);
        }

        return $this;
    }

    public function removeChildren(self $children): self
    {
        if ($this->childrens->removeElement($children)) {
            // set the owning side to null (unless already changed)
            if ($children->getParent() === $this) {
                $children->setParent(null);
            }
        }

        return $this;
    }

    public function getArticle(): ?NewsArticle
    {
        return $this->article;
    }

    public function setArticle(?NewsArticle $article): self
    {
        $this->article = $article;

        return $this;
    }
}
