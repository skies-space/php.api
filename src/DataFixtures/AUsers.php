<?php

namespace App\DataFixtures;

use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AUsers extends Fixture
{
    public function __construct(private UserRepository $repository, private UserPasswordEncoderInterface $encoder){}

    public function load(ObjectManager $manager)
    {
        $this->repository->register_admin(
            $_ENV['FIX_ADMIN_USERNAME'],
            $_ENV['FIX_ADMIN_EMAIL'],
            $_ENV['FIX_ADMIN_PASSWORD'],
            $this->encoder
        );

        $manager->flush();
    }
}
