<?php

namespace App\DataFixtures;

use App\Entity\NewsArticle;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use \Faker\Factory;

class NewsArticles extends Fixture
{
    public function __construct(
        private UserRepository $userRepo
    ){}

    public function load(ObjectManager $manager)
    {
        $user = $this->userRepo->findByEmail($_ENV['FIX_ADMIN_EMAIL']);
        $fnac = 100;
        $faker = Factory::create();
        for($i=0;$i<$fnac; $i++ ) {
            $article = (new NewsArticle())
                ->setPreviewImage('test')
                ->setPreviewText($faker->text(100))
                ->setDetailImage('test')
                ->setDetailText($faker->text(500))
                ->setTitle($faker->sentence)
                ->setPublishDate(new \DateTime())
                ->setAuthor($user);
//            dd($article);
            $manager->persist($article);
        }
        $manager->flush();
    }
}
