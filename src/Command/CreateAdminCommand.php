<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @property EntityManager $em
 */
class CreateAdminCommand extends Command
{
    protected static $defaultName = 'CreateAdmin';
    private EntityManagerInterface $em;
    private UserPasswordEncoderInterface $pass_encoder;
    public function __construct(EntityManagerInterface $doctrineEm, UserPasswordEncoderInterface $encoder)
    {
        $this->em = $doctrineEm;
        $this->pass_encoder = $encoder;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('name', InputArgument::REQUIRED, 'admin name')
            ->addArgument('mail', InputArgument::REQUIRED, 'admin mail')
            ->addArgument('password', InputArgument::OPTIONAL, 'optional, password')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $generator = new ComputerPasswordGenerator();
        $generator->setUppercase()
            ->setLowercase()
            ->setNumbers()
            ->setSymbols(false)
            ->setLength(random_int(5,12));
        $io = new SymfonyStyle($input, $output);
        $name = $input->getArgument('name');
        $mail = $input->getArgument('mail');
        $password = !empty($input->getArgument('password'))?$input->getArgument('password'):$generator->generatePassword();
        $register_result = $this->em->getRepository(User::class)->register_admin(
            $name,
            $mail,
            $password,
            $this->pass_encoder
        );


        if ($register_result) {
            $io->success('Admin registered');
            $io->info("Password: $password");
            $io->info("Email: $mail");
            $io->info("Name: $name");
        }
        else{
            $io->error("Admin already registered");
        }
        return Command::SUCCESS;
    }
}
