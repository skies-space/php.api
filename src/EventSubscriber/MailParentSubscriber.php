<?php
namespace App\EventSubscriber;

use App\Entity\NewsArticleComment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class MailParentSubscriber
{
    private MailerInterface $mailer;
    private EntityManagerInterface $em;
    private string $message = 'mail notification';
    private string $subject = 'mail notification';

    public function __construct(
        MailerInterface $mailer,
        EntityManagerInterface $entityManager,
        string $message = null,
        string $subject = null
    )
    {
        $this->mailer = $mailer;
        $this->em = $entityManager;
        if(!is_null($message)){
            $this->message = $message;
        }
        if(!is_null($subject)){
            $this->subject = $subject;
        }
    }

    public function postPersist(NewsArticleComment $comment)
    {
        $parent = $comment->getParent()?->getUser();
        if(!is_null($parent))
        {
            $this->em->initializeObject($parent);
            $email = (new Email())
                ->from('news.portal@test.local')
                ->to($parent->getEmail())
                ->subject($this->subject)
                ->text($this->message);
            $this->mailer->send($email);
        }
    }
}
