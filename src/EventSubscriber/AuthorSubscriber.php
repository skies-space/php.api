<?php
namespace App\EventSubscriber;

use App\Entity\NewsArticle;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class AuthorSubscriber
{
    public function __construct(private TokenStorageInterface $token){}

    public function prePersist(NewsArticle $article)
    {
        $author = $this->token->getToken()?->getUser();
        if(!is_null($author)){
            $article->setAuthor($author);
        }
    }
}
