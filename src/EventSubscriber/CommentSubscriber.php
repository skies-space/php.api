<?php
namespace App\EventSubscriber;

use App\Entity\NewsArticleComment;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class CommentSubscriber
{
    private ?UserInterface $commenter;

    public function __construct(TokenStorageInterface $token)
    {
        $this->commenter = $token->getToken()?->getUser();
    }

    public function prePersist(NewsArticleComment $posted_comment)
    {
        $posted_comment->setUser($this->commenter);
    }
}
