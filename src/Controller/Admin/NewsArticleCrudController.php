<?php
namespace App\Controller\Admin;

use App\Entity\NewsArticle;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\FileUploadType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class NewsArticleCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return NewsArticle::class;
    }


    public function configureFields(string $pageName): iterable
    {
        $field_list = [
            TextField::new('title'),
            TextField::new('previewText'),
            TextareaField::new('detailText'),
        ];
        $preview_image = ImageField::new('preview_image')
            ->setUploadDir($this->getParameter('app.uploads.preview_images.dir'))
            ->setBasePath($this->getParameter('app.uploads.preview_images.basepath'))
            ->setUploadedFileNamePattern('[contenthash].[extension]');
        $detail_image = ImageField::new('detail_image')
            ->setUploadDir($this->getParameter('app.uploads.detail_images.dir'))
            ->setBasePath($this->getParameter('app.uploads.detail_images.basepath'))
            ->setUploadedFileNamePattern('[contenthash].[extension]');
        if(($pageName === Action::EDIT)) {
            $preview_image->setRequired(false);
            $detail_image->setRequired(false);
        }
        $field_list = array_merge($field_list, [$preview_image, $detail_image]);

        if(($pageName === Action::NEW) || ($pageName === Action::EDIT)){
            $field_list[] = CollectionField::new('attachments')
                ->allowAdd(true)
                ->allowDelete()
                ->setEntryIsComplex(true)
                ->setEntryType(FileUploadType::class)
                ->setFormTypeOptions([
                    'by_reference'      => false,
                    'entry_options'     => [
                        'upload_dir'        => $this->getParameter('app.uploads.attachments.dir'),
                        'upload_filename'   => '[contenthash].[extension]'
                    ]
                ]);
        }
        return $field_list;
    }
}
