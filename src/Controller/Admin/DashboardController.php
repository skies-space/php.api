<?php
namespace App\Controller\Admin;

use App\Entity\NewsArticle;
use App\Entity\NewsArticleComment;
use App\Entity\User;
use App\Repository\UserRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

#[Route('/admin')]
#[IsGranted(["ROLE_ADMIN"])]
class DashboardController extends AbstractDashboardController
{
    public function  __construct(
        private UserRepository $userRepository
    ){}

    #[Route('', name:"admin_index")]
    public function index(): Response
    {
        return $this->render('/admin/dashboard.html.twig',
            [
                'dashboard_controller_filepath'=>'/',
                'dashboard_controller_class'=>'/',
                'showUserCounter' => true,
                'usersCount' => $this->userRepository->countUsers()
            ]);
    }

    #[Route('/login', name:"admin_dash_login")]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
         if ($this->getUser()) {
             return $this->redirectToRoute('admin_index');
         }
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('@EasyAdmin/page/login.html.twig', [
            'error'         => $error,
            'last_username' => $lastUsername,
            'translation_domain'    => 'admin',
//            'page_title'            => 'ACME login',
//            'username_label'        => 'Email',
//            'password_label'        => 'Password',
//            'sign_in_label'         => 'Log in',
            'csrf_token_intention'  => 'authenticate',
            'username_parameter'    => 'username',
            'password_parameter'    => 'password',
        ]);
    }

    #[Route('/logout', name:"app_logout")]
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Php Api');
    }
    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Users', 'fas fa-users', User::class);
        yield MenuItem::linkToCrud('Comments', 'fas fa-comments', NewsArticleComment::class);
        yield MenuItem::linkToCrud('Articles', 'fas fa-newspaper', NewsArticle::class);
    }
    public function configureUserMenu(UserInterface $user): UserMenu
    {
        return parent::configureUserMenu($user)->setAvatarUrl($this->getParameter('app.uploads.avatars.basepath').DIRECTORY_SEPARATOR.$user->getAvatar());
    }
    #[Route('/count/users', name:"admin_util_users_count", methods: ["POST", "GET"])]
    public function getUsersCount() : Response
    {
        return $this->json($this->userRepository->countUsers());
    }
}
