<?php
namespace App\Controller\Admin;

use App\Admin\Field\SetPasswordField;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureFields(string $pageName): iterable
    {
        $field_list = [
            TextField::new('username'),
            EmailField::new('email'),
            ImageField::new('avatar')
                ->setUploadDir($this->getParameter('app.uploads.avatars.dir'))
                ->setBasePath($this->getParameter('app.uploads.avatars.basepath'))
                ->setUploadedFileNamePattern('[contenthash].[extension]'),
        ];
        if(($pageName === Action::NEW) || ($pageName === Action::EDIT)){
            $field_list =
            array_merge(
                $field_list,
                [
                    ChoiceField::new('roles')
                        ->allowMultipleChoices()
                        ->autocomplete()
                        ->setChoices(User::getGroupsLabels()),
                    SetPasswordField::new('plainPassword')
                ]
            );
        }
        return $field_list;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityPermission(User::ROLE_ADMIN);
    }
}
