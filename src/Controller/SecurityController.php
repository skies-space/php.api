<?php
namespace App\Controller;

use App\Entity\User;
use App\Form\BanType;
use App\Form\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Form\FormInterface;

class SecurityController extends AbstractController
{
    #[Route('/register', name: 'app_register', requirements: ['_format'=>'json'], methods: ['POST'])]
    public function registerUser(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $form = $this->createForm(RegistrationFormType::class);
        $form->submit(!empty($request->request->all())?$request->request->all():$request->toArray());
        $form->handleRequest($request);
        if($form->isValid()){
            $register_result = $this->getDoctrine()->getManager()->getRepository(User::class)->register_user(
                $form->get('username')->getData(),
                $form->get('email')->getData(),
                $form->get('password')->getData(),
                $encoder
            );

            return $register_result?
                (new Response())->setContent('user registered'):
                (new Response())->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)->setContent('user already registered');
        }

        return (new Response())
            ->setStatusCode(Response::HTTP_CONFLICT)
            ->setContent(json_encode($this->getFormErrorsArray($form), JSON_THROW_ON_ERROR));
    }

    #[Route('/ban', name: 'app_user_ban', requirements: ['_format'=>'json'], methods: ['POST'])]
    public function banUser(Request $request, MailerInterface $mailer): Response
    {
        $this->denyAccessUnlessGranted(User::ROLE_ADMIN);
        $form = $this->createForm(BanType::class);
        $form->submit(!empty($request->request->all())?$request->request->all():$request->toArray());
        $form->handleRequest($request);
        if($form->isValid()){
            $user = $this->getDoctrine()->getManager()
                ->getRepository(User::class)->banByEmail($form->get('email')->getData());
            if(!is_null($user)){
                $email = (new Email())
                    ->from('news.portal@test.local')
                    ->to($user->getEmail())
                    ->subject('news portal ban')
                    ->text('U banned, sorry');
                $mailer->send($email);
                return (new Response())->setContent('user banned');
            }
            return (new Response())
                ->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
                ->setContent('Cant ban user! Call administrator!');
        }

        return (new Response())
            ->setStatusCode(Response::HTTP_CONFLICT)
            ->setContent(json_encode($this->getFormErrorsArray($form), JSON_THROW_ON_ERROR));
    }

    #[Route('/unban', name: 'app_user_unban', requirements: ['_format'=>'json'], methods: ['POST'])]
    public function unbanUser(Request $request, MailerInterface $mailer): Response
    {
        $this->denyAccessUnlessGranted(User::ROLE_ADMIN);
        $form = $this->createForm(BanType::class);
        $form->submit(!empty($request->request->all())?$request->request->all():$request->toArray());
        $form->handleRequest($request);
        if($form->isValid()){
            $user = $this->getDoctrine()->getManager()
                ->getRepository(User::class)->unbanByEmail($form->get('email')->getData());
            if(!is_null($user)){
                $email = (new Email())
                    ->from('news.portal@test.local')
                    ->to($user->getEmail())
                    ->subject('news portal ban')
                    ->text('U unbanned, post comments!');
                $mailer->send($email);
                return (new Response())->setContent('user unbanned');
            }
            return (new Response())
                ->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
                ->setContent('Cant unban user! Call administrator!');
        }


        return (new Response())
            ->setStatusCode(Response::HTTP_CONFLICT)
            ->setContent(json_encode($this->getFormErrorsArray($form), JSON_THROW_ON_ERROR));
    }

    protected function getFormErrorsArray(FormInterface $form) : array
    {
        $errors = [];
        foreach ($form->getErrors() as $error){
            $errors[] = $error->getMessage();
        }
        return $errors;
    }
}
