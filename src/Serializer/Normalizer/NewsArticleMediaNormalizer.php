<?php

namespace App\Serializer\Normalizer;

use App\Entity\NewsArticle;
use Psr\Container\ContainerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerInterface;

class NewsArticleMediaNormalizer implements NormalizerInterface, DenormalizerInterface, SerializerAwareInterface
{
    public function __construct(
        private NormalizerInterface $decorated,
        private ContainerInterface $container
    )
    {
        if (!$decorated instanceof DenormalizerInterface) {
            throw new \InvalidArgumentException(sprintf('The decorated normalizer must implement the %s.', DenormalizerInterface::class));
        }
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof NewsArticle;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        $data = $this->decorated->normalize($object, $format, $context);
        if (is_array($data)) {
            if(isset($data['detailImage']) && !empty($data['detailImage']))
                $data['detailImage']  = sprintf('%s%s',$this->container->getParameter('app.uploads.detail_images.basepath'), $data['detailImage']);

            if(isset($data['previewImage']) && !empty($data['previewImage']))
                $data['previewImage'] = sprintf('%s%s',$this->container->getParameter('app.uploads.preview_images.basepath'), $data['previewImage']);

            if(isset($data['attachments']) && !empty($data['attachments']))
                foreach ($data['attachments'] as $key => $datum)
                    $data['attachments'][$key] = sprintf('%s%s',$this->container->getParameter('app.uploads.attachments.basepath'), $datum);
//            $data['date'] = date(\DateTime::RFC3339);
        }

        return $data;
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return false;
    }

    public function denormalize($data, $class, $format = null, array $context = [])
    {
        return null;
    }

    public function setSerializer(SerializerInterface $serializer)
    {
        if($this->decorated instanceof SerializerAwareInterface) {
            $this->decorated->setSerializer($serializer);
        }
    }
}
