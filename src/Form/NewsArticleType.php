<?php

namespace App\Form;

use App\Entity\NewsArticle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class NewsArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title',
                TextType::class,
                [
                    'constraints'   => [new NotBlank()]
                ])
            ->add(
                'preview_text',
                TextType::class,
                [
                    'constraints'   => [new NotBlank()]
                ]
            )
            ->add(
                'detail_text',
                TextType::class,
                [
                    'constraints'   => [new NotBlank()]
                ]
            )
            ->add('publish_date')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => NewsArticle::class,
        ]);
    }
}
