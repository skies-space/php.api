<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }
    public function register_user(string $username, string $email, string $password, UserPasswordEncoderInterface $encoder): bool
    {
        $result = $this->createQueryBuilder('u')
            ->where('u.email = :mail')
            ->setParameter('mail',$email)
            ->getQuery()
            ->getOneOrNullResult();
        if(is_null($result)){
            $user = (new User())->setUsername($username)->setEmail($email);
            $user->setPassword($encoder->encodePassword($user, $password))->setRoles([USER::ROLE_READER, USER::ROLE_COMMENTATOR]);

            $this->_em->persist($user);
            $this->_em->flush();
            return true;
        }
        return false;
    }
    public function register_admin(string $username, string $email, string $password, UserPasswordEncoderInterface $encoder): bool
    {
        $result = $this->createQueryBuilder('u')
            ->where('u.email = :mail')
            ->setParameter('mail',$email)
            ->getQuery()
            ->getOneOrNullResult();
        if(is_null($result)){
            $user = (new User())->setUsername($username)->setEmail($email);
            $user->setPassword($encoder->encodePassword($user, $password))->setRoles([USER::ROLE_ADMIN]);

            $this->_em->persist($user);
            $this->_em->flush();
            return true;
        }
        return false;
    }

    public function findByEmail($value = null)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.email = :mail')
            ->setParameter('mail', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function banByEmail($mail = null): ?User
    {
        $user = $this->findByEmail($mail);
        if(!is_null($user)) {
            $this->BanUser($user);
        }
        return $user;
    }
    public function unbanByEmail($mail = null): ?User
    {
        $user = $this->findByEmail($mail);
        if(!is_null($user)) {
            $this->UnbanUser($user);
        }
        return $user;
    }
    public function BanUser(User $user):void
    {
        $user->setRoles([User::ROLE_READER]);
        $this->_em->persist($user);
        $this->_em->flush();
    }
    public function UnbanUser(User $user):void
    {
        $user->setRoles([User::ROLE_READER, User::ROLE_COMMENTATOR]);
        $this->_em->persist($user);
        $this->_em->flush();
    }
    public function countUsers() : ?int
    {
        $result = $this->createQueryBuilder('u')
            ->getQuery()
            ->getArrayResult();
        return is_array($result)?count($result):null;
    }
}
