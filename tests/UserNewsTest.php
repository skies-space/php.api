<?php
namespace App\Tests;

use App\Tests\Abstract\JwtAuthTestCase;
use Symfony\Component\HttpFoundation\Response;

class UserNewsTest extends JwtAuthTestCase
{

    public function testPosting(): void
    {
        $test_article = [
            'detailText'=>'2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text',
            'previewText' =>'2nd-article previewText',
            'title'=>'2nd-article'
        ];
        $client = static::createAuthorizedClient($_ENV['FIX_ADMIN_EMAIL'], $_ENV['FIX_ADMIN_PASSWORD']);
        $client->request(
            'POST',
            '/api/news',
            array_merge(['body' => json_encode($test_article)])
        );
        self::assertEquals(Response::HTTP_FORBIDDEN  ,$client->getResponse()->getStatusCode());
    }

    /**
     * @return array
     * @throws \JsonException
     */
    public function testListRequest(): array
    {
        $client = static::createAuthorizedClient($_ENV['FIX_ADMIN_EMAIL'], $_ENV['FIX_ADMIN_PASSWORD']);
        $client->request(
            'GET',
            '/api/news',
            ['query' => ['pagination'=>false]]
        );
        self::assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode(), message: 'wrong server response');
        $result_news_list = json_decode($client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);
        self::assertGreaterThan(0, count($result_news_list));
        return $result_news_list[0];
    }

    /**
     * @depends testListRequest
     * @param array $respond_item_array
     * @return array
     * @throws \JsonException
     */
    public function testItemRequest(array $respond_item_array): array
    {
        $client = static::createAuthorizedClient($_ENV['FIX_ADMIN_EMAIL'], $_ENV['FIX_ADMIN_PASSWORD']);
        $client->request(
            'GET',
            '/api/news/'.$respond_item_array['id'],
            ['query' => ['pagination'=>false]]
        );
        self::assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode(), message: 'wrong server response');
        $respond_item = json_decode($client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);
        self::assertEquals($respond_item_array, $respond_item);
        return $respond_item;
    }

    /**
    * @depends testItemRequest
    * @param array $respond_item_array
    * @return array
    * @throws \JsonException
    */
    public function testItemUpdate(array $respond_item_array): array
    {
        $test_article = [
            'detailText'=>'2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text,2nd-article test_text',
            'previewText' =>'2nd-article previewText',
            'title'=>'2nd-article'
        ];
        $client = static::createAuthorizedClient($_ENV['FIX_ADMIN_EMAIL'], $_ENV['FIX_ADMIN_PASSWORD']);
        $client->request(
            'PUT',
            '/api/news/'.$respond_item_array['id'],
            ['body' => json_encode($test_article, JSON_THROW_ON_ERROR)]
        );
        self::assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode(), message: 'wrong server response');

        return $respond_item_array;
    }
    /**
     * @depends testItemUpdate
     * @param array $respond_item_array
     * @return array
     * @throws \JsonException
     */
    public function testItemDelete(array $respond_item_array): array
    {
        $client = static::createAuthorizedClient($_ENV['FIX_ADMIN_EMAIL'], $_ENV['FIX_ADMIN_PASSWORD']);
        $client->request(
            'DELETE',
            '/api/news/'.$respond_item_array['id']
        );
        self::assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode(), message: 'wrong server response');

        return $respond_item_array;
    }

    /**
     * @depends testItemUpdate
     * @param array $respond_item_array
     * @return array
     * @throws \JsonException
     */
    public function testItemComment(array $respond_item_array): array
    {
        $client = static::createAuthorizedClient($_ENV['FIX_ADMIN_EMAIL'], $_ENV['FIX_ADMIN_PASSWORD']);
        $client->request(
            'DELETE',
            '/api/news/'.$respond_item_array['id']
        );
        self::assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode(), message: 'wrong server response');

        return $respond_item_array;
    }

}
