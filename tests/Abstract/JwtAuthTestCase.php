<?php
namespace App\Tests\Abstract;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;

class JwtAuthTestCase extends ApiTestCase
{
    protected static array $default_options = [
        'headers' => [
            'accept' => 'application/json',
            'content-type'=>'application/json'
        ]
    ];

    protected static function createAuthorizedClient($email, $password): Client
    {
        $kernelOptions = [];
        $defaultOptions = [
            'headers'  => self::$default_options['headers'],
            'base_uri' => $_ENV['TESTING_HOST']
        ];
        $client = parent::createClient($kernelOptions, $defaultOptions);
        $client->request(
            'POST',
            '/login',
            [
                'body' => json_encode(['email' => $email, 'password' => $password,], JSON_THROW_ON_ERROR)
            ]
        );
        $data = json_decode($client->getResponse()->getContent(), true);
        $client->setDefaultOptions(
            array_merge(
                self::$default_options,
                [
                    'auth_bearer'=>$data['token']
                ]
            )
        );
        return $client;
    }
    protected static function createAnonymousClient(): Client
    {
        $kernelOptions = [];
        $defaultOptions = [
            'headers'  => self::$default_options['headers'],
            'base_uri' => $_ENV['TESTING_HOST']
        ];
        return parent::createClient($kernelOptions, $defaultOptions);
    }
}
